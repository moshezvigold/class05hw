#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>

enum shapes {circle, arrow, triangle, rectangle};
enum actions {moveShape, details, removeShape};

class Menu
{
public:

	Menu();
	~Menu();
	int startMenu();
	void addShape();
	void clearShapes();
	void doStuff();

private: 
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
	std::vector<Shape*> _shapes;

	Shape* createArrow();
	Shape* createCircle();
	Shape* createTriangle();
	Shape* createRectangle();
};
