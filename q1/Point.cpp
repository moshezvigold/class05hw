#include "Point.h"

Point::Point()
{
}

Point::Point(double x, double y):
	_x(x),
	_y(y)
{
}

Point::Point(const Point& other):
	_x(other.getX()),
	_y(other.getY())
{
}

Point::~Point()
{
}

Point Point::operator+(const Point& other) const
{
	return Point(this->_x + other._x, this->_y + other._y);
}

Point& Point::operator+=(const Point& other)
{
	this->_x += other._x;
	this->_y += other._y;

	return *this;
}

double Point::getX() const
{
	return this->_x;
}
double Point::getY() const
{
	return this->_y;
}

double Point::distance(const Point& other) const
{
	return sqrt(pow(this->_x - other.getX(), 2) + pow(this->_x - other.getY(), 2));
}