#include "Menu.h"
enum choices {newShape = 0, doStuff = 1, deleteAll = 2, finish = 3};

void main()
{
	int userChoice = 0;
	Menu menu = Menu();
	
	while (userChoice != finish)
	{
		userChoice = menu.startMenu();

		if (userChoice != finish)
		{
			switch (userChoice)
			{
			case  newShape:
				menu.addShape();
				system("cls");
				break;

			case doStuff:
				menu.doStuff();
				break;

			case deleteAll:
				menu.clearShapes();
				break;

			default:
				break;
			}
		}
	}

	menu.clearShapes();
}
/*
point - also distance
arrow
triangle
rectangle
circle
*/

/*
Point a = Point(3, 4);
Point b = Point(2, 5);
Point c = Point(1, 2);
Arrow arrow = Arrow(a, b, "stuff", "Arrow");
*/