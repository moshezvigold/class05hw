#pragma once
#include "Polygon.h"
#define POINTS_IN_TRIANGLE 3

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name);
	virtual ~Triangle();

	//drawing functions
	void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	
	virtual double getArea() const;
	virtual double getPerimeter() const;
	virtual void move(const Point& other);
};