#include "Menu.h"

Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

int Menu::startMenu()
{
	int choice = 0;

	cout << "Enter 0 to add a new shape.\nEnter 1 to modify or get information from a current shape.\nEnter 2 to delete all of the shapes.\nEnter 3 to exit." << endl;
	cin >> choice;
	system("cls");

	return choice;
}

void Menu::addShape()
{
	int shapeChoice = 0;
	string name;
	Shape* shape;

	cout << "Enter 0 to add a circle.\nEnter 1 to add an arrow.\nEnter 2 to add a triangle.\nEnter 3 to add a rectangle." << endl;
	cin >> shapeChoice;
	
	switch (shapeChoice)
	{
	case circle:
		shape = createCircle();
		shape->draw(*this->_disp, *this->_board);
		this->_shapes.push_back(shape);
		break;

	case arrow:
		shape = createArrow();
		shape->draw(*this->_disp, *this->_board);
		this->_shapes.push_back(shape);
		break;

	case triangle:
		shape = createTriangle();
		shape->draw(*this->_disp, *this->_board);
		this->_shapes.push_back(shape);
		break;// add if it creates a arrow some kind of error correction

	case rectangle:
		shape = createRectangle();
		shape->draw(*this->_disp, *this->_board);
		this->_shapes.push_back(shape);
		break;

	default:
		break;
	}

	system("cls");
}

/*
The function clears all the drawings of the exsisting shapes and then frees their memory.
*/
void Menu::clearShapes()
{
	if (!this->_shapes.empty()) 
	{
		for (size_t i = 0; i < this->_shapes.size(); i++)
		{
			this->_shapes[i]->clearDraw(*this->_disp, *this->_board);
			delete this->_shapes[i];
			this->_shapes.erase(this->_shapes.begin() + i);
		}
	}
}

/*
The function prints a menu(options are: print the details, move the shape and delete the shape) and does whatever the user chooses.
*/
void Menu::doStuff()
{
	int shapeIndex = 0, choice = 0;
	double x = 0, y = 0;
	
	if (!this->_shapes.empty())
	{
		for (size_t i = 0; i < this->_shapes.size(); i++)//printing out a list of the shapes in the vector
		{
			cout << "Enter " << i << " for " << this->_shapes[i]->getName() << "(" << this->_shapes[i]->getType() << ")" << endl;
		}

		cin >> shapeIndex;

		//getting the users choice
		cout << "Enter 0 to move the shape\nEnter 1 to get its details.\nEnter 2 to remove the shape." << endl;
		cin >> choice;

		switch (choice)
		{
		case moveShape:
			system("cls");
			cout << "Please enter the X moving scale : ";
			cin >> x;
			cout << "Please enter the Y moving scale : ";
			cin >> y;
			this->_shapes[shapeIndex]->move(Point(x, y));
			break;

		case details:
			this->_shapes[shapeIndex]->printDetails();
			break;

		case removeShape:
			this->_shapes[shapeIndex]->clearDraw(*this->_disp, *this->_board);
			delete this->_shapes[shapeIndex];
			this->_shapes.erase(this->_shapes.begin() + shapeIndex);
			break;

		default:
			break;
		}
		system("cls");
	}
}

/*
All the functions bellow create a certain shape from user input and retrun a pointer to that shape object.
*/
Shape* Menu::createArrow()
{
	double xTemp, yTemp = 0;
	Point a, b;
	string name;

	cout << "Enter the X of point number : 1" << endl;
	cin >> xTemp;
	cout << "Enter the Y of point number : 1" << endl;
	cin >> yTemp;
	a = Point(xTemp, yTemp);
	cout << "Enter the X of point number : 2" << endl;
	cin >> xTemp;
	cout << "Enter the Y of point number : 2" << endl;
	cin >> yTemp;
	b = Point(xTemp, yTemp);
	cout << "Enter the name of the shape" << endl;
	cin >> name;
	
	return new Arrow(a, b, "Arrow", name);
}

Shape* Menu::createCircle()
{
	double radius = 0, xTemp = 0, yTemp = 0;
	Point center;
	string name;

	cout << "Please enter X :" << endl;
	cin >> xTemp;
	cout << "Please enter Y :" << endl;
	cin >> yTemp;
	center = Point(xTemp, yTemp);
	cout << "Please enter radius :" << endl;
	cin >> radius;
	cout << "Please enter the name of the shape :" << endl;
	cin >> name;

	return new Circle(center, radius, "Circle", name);
}

Shape* Menu::createTriangle()
{
	double xTemp = 0, yTemp = 0;
	Point a, b, c;
	string name;

	cout << "Enter the X of point number : 1" << endl;
	cin >> xTemp;
	cout << "Enter the Y of point number : 1" << endl;
	cin >> yTemp;
	a = Point(xTemp, yTemp);

	cout << "Enter the X of point number : 2" << endl;
	cin >> xTemp;
	cout << "Enter the Y of point number : 2" << endl;
	cin >> yTemp;
	b = Point(xTemp, yTemp);

	cout << "Enter the X of point number : 3" << endl;
	cin >> xTemp;
	cout << "Enter the Y of point number : 3" << endl;
	cin >> yTemp; 
	c = Point(xTemp, yTemp);

	cout << "Enter the name of the shape :" << endl;
	cin >> name;

	return new Triangle(a, b, c, "Triangle", name);
}

Shape* Menu::createRectangle()
{
	double x, y, length, width;
	string name;
	Point a;

	cout << "Enter the X of the top left corner :" << endl;
	cin >> x;
	cout << "Enter the Y of the top left corner :" << endl;
	cin >> y;
	a = Point(x, y);
	cout << "Please enter the length of the shape : " << endl;
	cin >> length;
	cout << "Please enter the width of the shape :" << endl;
	cin >> width;
	cout << "Enter the name of the shape :" << endl;
	cin >> name;

	return new myShapes::Rectangle(a, length, width, "Rectangle", name);
}