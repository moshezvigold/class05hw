#include "Shape.h"

Shape::Shape(const string& name, const string& type):
	_name(name),
	_type(type)
{
}

Shape::~Shape()
{
}

void Shape::printDetails() const
{
	cout << this->_type << "  " << this->_name << "    " << this->getArea() << "  " << this->getPerimeter() << endl;
	system("pause");
}

string Shape::getType() const
{
	return this->_type;
}

string Shape::getName() const
{
	return this->_name;
}