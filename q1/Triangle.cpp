#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name):
	Polygon(name, type)
{
	this->_points.reserve(3);
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);
}

Triangle::~Triangle()
{
}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}

double Triangle::getArea() const
{
	//first i am getting height multiplying by the length of the base and then dividing by 2
	return (this->_points[A].getY() + this->_points[C].getY()) * this->_points[A].distance(this->_points[B])/2;
}

double Triangle::getPerimeter() const
{
//distance between d(a and b) + d(b and c) + d(c and a)
	return this->_points[A].distance(this->_points[B]) + this->_points[B].distance(this->_points[C]) + this->_points[C].distance(this->_points[A]);
}

void Triangle::move(const Point& other)
{
	for (size_t i = 0; i < POINTS_IN_TRIANGLE; i++)
	{
		this->_points[i] += other;
	}
}